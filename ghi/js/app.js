function createCard(title, description, pictureUrl, starts, ends, name) {
  return `
        <div class="card shadow p-3 mb-5">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${title}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${name}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer text-muted">
          ${new Date(starts).toLocaleDateString()} -
          ${new Date(ends).toLocaleDateString()}
        </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Invalid response")
      } else {
        const data = await response.json();
        const column = document.querySelectorAll('.col');
        let index = 0

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const name = details.conference.location.name;
                const html = createCard(title, description, pictureUrl, starts, ends, name);
                column[index].innerHTML += html;
                if (index === 2) {
                  index = 0
                }else {
                  index++
              }

          }
        }

      }
    } catch (e) {
      console.error(e);
      // Figure out what to do if an error is raised
    }

  });
